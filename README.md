﻿
<h1 align="center">
    <img src="https://i.imgur.com/ynIcEhG.png">
</h1>
<p align="center">
<sup>
<b>Cálculo IMC é uma simples calculadora de Indice de Massa Corporal de um ser humano</b>
</sup>
</p>

## Bibliotecas Usadas

  * [JFoenix](https://github.com/jfoenixadmin/JFoenix) - JavaFX Material Design Library
  * [FontawesomeFX](https://bitbucket.org/Jerady/fontawesomefx) - Icon library

## Está usando Cálculo IMC?

Sinta-se à vontade para enviar qualquer dúvida ou sugestão sobre o projeto para o seguinte <a href="mailto:elvisdasilvademenezes@hotmail.com" target="_top">email</a>, para que eu possa ajudá-lo.
* Não se esqueça, todas as contribuições são bem vindas. Não hesite em adicionar suas próprias contribuições ao Cálculo IMC :)


**NOTE** : Cálculo IMC foi desenvolvido usando **jre1.8.0_171** and **jdk1.8.0_171**.

## Como eu uso Cálculo IMC?
Você pode baixar o código fonte e construí-lo usando qualquer IDE. Irá ser gerado um imc.jar sob a pasta imc / dist. Para usar o Cálculo IMC, importe as bibliotecas fontawesomefx-8.1.1.jar e jfoenix-8.0.4.jar para o seu projeto.

## Screenshots

<p align="left">
  <img src=https://i.imgur.com/c8MgJhM.png>
  <img src=https://i.imgur.com/jzYZKFs.png><br>
  <img src=https://i.imgur.com/hrSWCF5.png>
</p>
