/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imc;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 * @since 04 July 2018
 * @author Elvis
 */
public class FXMLMainController implements Initializable {
    
    @FXML
    private Label lblResult;
    @FXML
    private Label lblClass;
    @FXML
    private Label lblCons;
    @FXML
    private Label lbldGrau;
    @FXML
    private Label lblIMC;
    @FXML
    private JFXTextField txtAltura;
    @FXML
    private JFXTextField txtPeso;
    @FXML
    private JFXButton btnCalc;
    
    /*double x, y;
    
    @FXML
    void drageed(MouseEvent event) {
        
        Node node = (Node) event.getSource();
        Stage stage = (Stage) node.getScene().getWindow();
        stage.setX(event.getScreenX() -x);
        stage.setY(event.getScreenY() -y);
    }
    
    @FXML
    void pressed(MouseEvent event) {
        x = event.getSceneX();
        y = event.getSceneY();
    }
    */
    
    @FXML
    private void calcButtonAction(ActionEvent event) {
        lbldGrau.setVisible(false); //Setando a visibilidade do label lbldGrau para false na inicialização da primeira tela;
        
        float altura = (Float.parseFloat(txtAltura.getText())); //Variável 'altura' que recebe os valores que o user digita no JFXTextField;
        float peso = (Float.parseFloat(txtPeso.getText())); //Variável 'peso' que recebe os valores que o user digita no JFXTextField;
        
        float result = peso/(altura*altura); //Variável que faz o cálculo do IMC;
        
        lblResult.setText(String.valueOf(result)); //Setando o valor do label lblResult para o valor do cálculo feito na variável 'result';

        //Estrutura de controle 'if' que verifica em que situação o user se encontra em relação a tabela do IMC;
        if(result<16){
            lblIMC.setText("<16"); //Setando o valor do label lblIMC para o valor que condiz a condição;
            lblClass.setText("Você está com: Magreza grave"); //Setando o valor do label lblClass para a classificação que condiz a condição;
            lblCons.setText("Insuficiência cardíaca, anemia grave,\nenfraquecimento do sistema imunológico"); //Setando o valor do label lblCons para a possível consequência que condiz a condição;
            lbldGrau.setVisible(false); //Setando a visibilidade do label lbldGrau para false dependendo da condição;
        }else if(result>16 && result<17){
            lblIMC.setText(">16 & <17"); //Setando o valor do label lblIMC para o valor que condiz a condição;
            lblClass.setText("Você está com: Magreza moderada"); //Setando o valor do label lblClass para a classificação que condiz a condição;
            lblCons.setText("Infertilidade, queda de cabelo, falta da menstruação"); //Setando o valor do label lblCons para a possível consequência que condiz a condição;
            lbldGrau.setVisible(false); //Setando a visibilidade do label lbldGrau para false dependendo da condição;
        }else if(result>17 && result<18.5){
            lblIMC.setText(">17 & <18"); //Setando o valor do label lblIMC para o valor que condiz a condição;
            lblClass.setText("Você está com: Magreza leve"); //Setando o valor do label lblClass para a classificação que condiz a condição;
            lblCons.setText("Estresse, ansiedade, fadiga"); //Setando o valor do label lblCons para a possível consequência que condiz a condição;
            lbldGrau.setVisible(false); //Setando a visibilidade do label lbldGrau para false dependendo da condição;
        }else if(result>18.5 && result<25){
            lblIMC.setText(">18.5 & <25"); //Setando o valor do label lblIMC para o valor que condiz a condição;
            lblClass.setText("Você está: Saudável"); //Setando o valor do label lblClass para a classificação que condiz a condição;
            lblCons.setText("Menor risco para doenças"); //Setando o valor do label lblCons para a possível consequência que condiz a condição;
            lbldGrau.setVisible(false); //Setando a visibilidade do label lbldGrau para false dependendo da condição;
        }else if(result>25 && result<30){
            lblIMC.setText(">25 & <30"); //Setando o valor do label lblIMC para o valor que condiz a condição;
            lblClass.setText("Você está com: Sobrepeso"); //Setando o valor do label lblClass para a classificação que condiz a condição;
            lblCons.setText("Fadiga, varizes, má circulação"); //Setando o valor do label lblCons para a possível consequência que condiz a condição;
            lbldGrau.setVisible(false); //Setando a visibilidade do label lbldGrau para false dependendo da condição;
        }else if(result>30 && result<35){
            lblIMC.setText(">30 & <35"); //Setando o valor do label lblIMC para o valor que condiz a condição;
            lblClass.setText("Você está com: Obesidade Grau I"); //Setando o valor do label lblClass para a classificação que condiz a condição;
            lblCons.setText("Diabetes, infarto, angina, aterosclerose"); //Setando o valor do label lblCons para a possível consequência que condiz a condição;
            lbldGrau.setVisible(false); //Setando a visibilidade do label lbldGrau para false dependendo da condição;
        }else if(result>35 && result<40){
            lblIMC.setText(">35 & <40"); //Setando o valor do label lblIMC para o valor que condiz a condição;
            lblClass.setText("Você está com: Obesidade Grau II"); //Setando o valor do label lblClass para a classificação que condiz a condição;
            lbldGrau.setVisible(true); //Setando a visibilidade do label lbldGrau para true dependendo da condição;
            lbldGrau.setText("Severa!"); //Setando o valor do label lbldGrau para o grau correspondente;
            lblCons.setText("Apneia do sono, falta de ar");//Setando o valor do label lblCons para a possível consequência que condiz a condição;
        }else if(result>40){
            lblIMC.setText(">40"); //Setando o valor do label lblIMC para o valor que condiz a condição;
            lblClass.setText("Você está com: Obesidade Grau III"); //Setando o valor do label lblClass para a classificação que condiz a condição;
            lbldGrau.setVisible(true); //Setando a visibilidade do label lbldGrau para true dependendo da condição;
            lbldGrau.setText("Mórbida!"); //Setando o valor do label lbldGrau para o grau correspondente;
            lblCons.setText("Refluxo, infarto, AVC, dificuldade \nde locomoção, escaras"); //Setando o valor do label lblCons para a possível consequência que condiz a condição;
        }else{
            lblIMC.setText("<>"); //Zerando valor do label lblIMC para o padrão caso o digitado não corresponder as regras do software;
            lblClass.setText("Indisponível!"); //Setando valor do label lblIClass para indisponível caso o digitado não corresponder as regras do software;
            lblCons.setText("<>"); ////Zerando valor do label lblCons para o padrão caso o digitado não corresponder as regras do software;
        }
        
    }
    
    @FXML
    private void clearButtonAction(ActionEvent event){ //Ação do botão 'Limpar' responsável por chamar o método clear;
            clear();
        }
    
    @FXML 
    private void aboutButtonAction(ActionEvent event) throws IOException{ //Ação do botão 'Sobre' responsável por criar a scene 'Sobre - Cálculo IMC';
        Parent parent;
        parent = FXMLLoader.load(getClass().getResource("FXMLAbout.fxml"));
        Stage stage = new Stage();
        Scene scene = new Scene(parent);
        stage.setTitle("Sobre - Cálculo IMC"); //Alterando o título do  scene 'Sobre - Cálculo IMC'
        stage.initStyle(StageStyle.UTILITY);
        stage.setResizable(false); //Desabilitando o resizable do scene;
        stage.setScene(scene);
        stage.show(); //Chamando o scene 'Ajuda - Cálculo IMC';
        
    }
    
    @FXML
    private void idosoButtonAction(ActionEvent event) throws IOException{ //Ação do botão 'Idoso' responsável por criar a scene 'Idoso - Cálculo IMC';
        Parent parent;
        parent = FXMLLoader.load(getClass().getResource("FXMLIdoso.fxml"));
        Stage stage = new Stage();
        Scene scene = new Scene(parent);
        stage.setTitle("Idoso - Cálculo IMC"); //Alterando o título do scene 'Idoso - Cálculo IMC'
        stage.initStyle(StageStyle.UTILITY);
        stage.setResizable(false); //Desabilitando o resizable do scene;
        stage.setScene(scene);
        stage.show(); //Chamando o scene 'Idoso - Cálculo IMC';
    }
    
    @FXML
    public void clear(){ //Método 'clear' responsável por setar os valores dos JFXTextFields e dos labels para o padrão;
        lblIMC.setText("<>");
        lblResult.setText("<>");
        lblClass.setText("<>");
        lblCons.setText("<>");
        lbldGrau.setText(""); //Setando o valor do lbldGrau para 'null';
        lbldGrau.setVisible(false); //Setando a visibilidade do label lbldGrau para false;
        txtAltura.setText(""); //Setando o valor do JFXTextField 'altura' para 'null';
        txtPeso.setText(""); //Setando o valor do JFXTextField 'peso' para 'null';
        
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
}
